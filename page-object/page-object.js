"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
var resemble = require('node-resemble-js');
const fs = require('fs');
let idScreenshot;
process.argv.forEach(function (val, index, array) {
    // console.log("From page object Key value", val);
    if (val.includes("--id")) {
        let splits = val.split("=");
        idScreenshot = splits[1];
        // console.log("Id for execution", splits);
        // console.log("Id for execution", idScreenshot);
    }
});
class Page {
    constructor() {
        this.screenCounter = 0;
    }
    defineVersion(version) {
        console.log("define version ", version);
        this.version = version;
        let folder = "screenshots/" + this.version;
        if (!fs.existsSync(folder)) {
            fs.mkdirSync(folder);
        }
    }
    sendTextFocused(text) {
        browser.keys(text);
    }
    goToUrl(url) {
        browser.url(url);
    }
    findByClass(elementClass) {
        return __awaiter(this, void 0, void 0, function* () {
            browser.waitForVisible("." + elementClass, 10000);
            browser.waitForEnabled("." + elementClass, 10000);
            return yield browser.element("." + elementClass);
        });
    }
    findByName(elementName) {
        return __awaiter(this, void 0, void 0, function* () {
            browser.waitForVisible('[name="' + elementName + '"]', 10000);
            browser.waitForEnabled('[name="' + elementName + '"]', 10000);
            return yield browser.element('[name="' + elementName + '"]');
        });
    }
    findById(elementId) {
        return __awaiter(this, void 0, void 0, function* () {
            browser.waitForVisible('#' + elementId + '', 10000);
            browser.waitForEnabled('#' + elementId + '', 10000);
            return yield browser.element('#' + elementId + '');
        });
    }
    pause(time) {
        return __awaiter(this, void 0, void 0, function* () {
            browser.pause(time);
        });
    }
    findByAttribute(attribute, value) {
        return __awaiter(this, void 0, void 0, function* () {
            browser.waitForVisible('[' + attribute + '*="' + value + '"]', 10000);
            browser.waitForEnabled('[' + attribute + '*="' + value + '"]', 10000);
            return yield browser.element('[' + attribute + '*="' + value + '"]');
        });
    }
    findAllByAttribute(attribute, value) {
        return __awaiter(this, void 0, void 0, function* () {
            browser.waitForVisible('[' + attribute + '*="' + value + '"]', 10000);
            browser.waitForEnabled('[' + attribute + '*="' + value + '"]', 10000);
            return yield browser.elements('[' + attribute + '*="' + value + '"]');
        });
    }
    findWithBaseByAttribute(attribute, value, containerAttribute, containerValue) {
        return __awaiter(this, void 0, void 0, function* () {
            browser.waitForVisible('[' + containerAttribute + '*="' + containerValue + '"]', 10000);
            browser.waitForEnabled('[' + containerAttribute + '*="' + containerValue + '"]', 10000);
            let container = browser.element('[' + containerAttribute + '*="' + containerValue + '"]');
            container.waitForVisible('[' + attribute + '*="' + value + '"]', 10000);
            container.waitForEnabled('[' + attribute + '*="' + value + '"]', 10000);
            return yield container.element('[' + attribute + '*="' + value + '"]');
        });
    }
    findByContent(tag, content) {
        return __awaiter(this, void 0, void 0, function* () {
            browser.waitForVisible(tag + '=' + content, 10000);
            browser.waitForEnabled(tag + '=' + content, 10000);
            return yield browser.element(tag + '=' + content);
        });
    }
    acceptAlert() {
        return __awaiter(this, void 0, void 0, function* () {
            yield browser.alertAccept();
        });
    }
    takeScreenshot() {
        return __awaiter(this, void 0, void 0, function* () {
            const screenshot = "screenshots/" + this.version + "/" + this.screenCounter + ".png";
            console.log("Take screemshot ", screenshot);
            console.log("Total screemshots ", this.screenCounter);
            yield browser.saveScreenshot(screenshot);
            this.screenCounter = this.screenCounter + 1;
            let counting = { version: this.version, count: this.screenCounter };
            let json = JSON.stringify(counting);
            fs.writeFile('data/versions/' + this.version + '.json', json, 'utf8', function (err) {
                console.log("The json file version status was saved", counting);
            });
        });
    }
    validateVersions(version) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    console.log("Current version", this.version);
                    console.log("Version to compare", version);
                    console.log("asdasdasadasdsad");
                    console.log(fs.readFileSync('data/versions/' + this.version + '.json', 'utf8'));
                    var counting = JSON.parse(fs.readFileSync('data/versions/' + this.version + '.json', 'utf8'));
                    var processItems = function (index, baseVersion, compareVersion) {
                        console.log("procesing", index, baseVersion, compareVersion);
                        if (index >= 0) {
                            const initScreenshot = "screenshots/" + baseVersion + "/" + index + ".png";
                            const finalScreenshot = "screenshots/" + compareVersion + "/" + index + ".png";
                            const changesScreenshot = "screenshots/changes_" + baseVersion + "_" + compareVersion + "_" + index + ".png";
                            resemble(initScreenshot).compareTo(finalScreenshot).onComplete(function (data) {
                                return __awaiter(this, void 0, void 0, function* () {
                                    yield fs.writeFile(changesScreenshot, data.getDiffImageAsJPEG());
                                    var jsonData = JSON.stringify(data);
                                    yield fs.writeFile("data/comparisons/" + baseVersion + "_" + compareVersion + "_" + index + ".json", jsonData);
                                    processItems(index - 1, baseVersion, compareVersion);
                                });
                            });
                        }
                        else {
                            fs.unlinkSync('data/versions/' + baseVersion + '.json');
                            fs.unlinkSync('data/versions/' + compareVersion + '.json');
                            resolve("¡Éxito!");
                        }
                    };
                    processItems(counting.count - 1, this.version, version);
                }, 2000);
            });
        });
    }
    // public async takeInitialScreenshot() {
    //     const initScreenshot = "screenshots/initial_" + idScreenshot + ".png";
    //     await browser.saveScreenshot(initScreenshot);
    // }
    // public async takeFinalScreenshot() {
    //     const finalScreenshot = "screenshots/final_" + idScreenshot + ".png";
    //     await browser.saveScreenshot(finalScreenshot);
    // }
    validateScreenshots() {
        return __awaiter(this, void 0, void 0, function* () {
            // console.log("fin validateScreenshots");
            return new Promise((resolve, reject) => {
                // console.log("validateScreenshots");
                const initScreenshot = "screenshots/initial_" + idScreenshot + ".png";
                const finalScreenshot = "screenshots/final_" + idScreenshot + ".png";
                const changesScreenshot = "screenshots/changes_" + idScreenshot + ".png";
                resemble(initScreenshot).compareTo(finalScreenshot).onComplete(function (data) {
                    return __awaiter(this, void 0, void 0, function* () {
                        yield fs.writeFile(changesScreenshot, data.getDiffImageAsJPEG());
                        var jsonData = JSON.stringify(data);
                        yield fs.writeFile("data/" + idScreenshot + ".json", jsonData);
                        resolve("¡Éxito!");
                    });
                });
            });
        });
    }
}
exports.Page = Page;
