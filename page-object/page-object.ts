var resemble = require('node-resemble-js');
const fs = require('fs');
let idScreenshot;
process.argv.forEach(function (val, index, array) {
    // console.log("From page object Key value", val);
    if (val.includes("--id")) {
        let splits = val.split("=");
        idScreenshot = splits[1];
        // console.log("Id for execution", splits);
        // console.log("Id for execution", idScreenshot);
    }
});

export class Page {

    screenCounter = 0;
    version;

    public defineVersion(version: string): any {
        console.log("define version ", version);
        this.version = version;
        let folder = "screenshots/" + this.version;
        if (!fs.existsSync(folder)) {
            fs.mkdirSync(folder);
        }
    }

    public sendTextFocused(text: string) {
        browser.keys(text);
    }

    public goToUrl(url: string) {
        browser.url(url);
    }
    public async findByClass(elementClass: string) {
        browser.waitForVisible("." + elementClass, 10000);
        browser.waitForEnabled("." + elementClass, 10000);
        return await browser.element("." + elementClass);
    }
    public async findByName(elementName: string) {
        browser.waitForVisible('[name="' + elementName + '"]', 10000);
        browser.waitForEnabled('[name="' + elementName + '"]', 10000);
        return await browser.element('[name="' + elementName + '"]');
    }

    public async findById(elementId: string) {
        browser.waitForVisible('#' + elementId + '', 10000);
        browser.waitForEnabled('#' + elementId + '', 10000);
        return await browser.element('#' + elementId + '');
    }

    public async pause(time: number) {
        browser.pause(time)
    }

    public async findByAttribute(attribute: string, value: string) {
        browser.waitForVisible('[' + attribute + '*="' + value + '"]', 10000);
        browser.waitForEnabled('[' + attribute + '*="' + value + '"]', 10000);
        return await browser.element('[' + attribute + '*="' + value + '"]');
    }
    public async findAllByAttribute(attribute: string, value: string) {
        browser.waitForVisible('[' + attribute + '*="' + value + '"]', 10000);
        browser.waitForEnabled('[' + attribute + '*="' + value + '"]', 10000);
        return await browser.elements('[' + attribute + '*="' + value + '"]');
    }

    public async findWithBaseByAttribute(attribute: string, value: string, containerAttribute: string, containerValue: string) {
        browser.waitForVisible('[' + containerAttribute + '*="' + containerValue + '"]', 10000);
        browser.waitForEnabled('[' + containerAttribute + '*="' + containerValue + '"]', 10000);
        let container = browser.element('[' + containerAttribute + '*="' + containerValue + '"]');
        container.waitForVisible('[' + attribute + '*="' + value + '"]', 10000);
        container.waitForEnabled('[' + attribute + '*="' + value + '"]', 10000);
        return await container.element('[' + attribute + '*="' + value + '"]');
    }

    public async findByContent(tag: string, content: string) {
        browser.waitForVisible(tag + '=' + content, 10000);
        browser.waitForEnabled(tag + '=' + content, 10000);
        return await browser.element(tag + '=' + content);
    }

    public async acceptAlert() {
        await browser.alertAccept();
    }

    public async takeScreenshot() {
        const screenshot = "screenshots/" + this.version + "/" + this.screenCounter + ".png";
        console.log("Take screemshot ", screenshot);
        console.log("Total screemshots ", this.screenCounter);
        await browser.saveScreenshot(screenshot);
        this.screenCounter = this.screenCounter + 1;
        let counting = { version: this.version, count: this.screenCounter };
        let json = JSON.stringify(counting);
        fs.writeFile('data/versions/' + this.version + '.json', json, 'utf8', function (err) {
            console.log("The json file version status was saved", counting);
        });
    }

    public async validateVersions(version: string) {
        return new Promise((resolve, reject) => {

            setTimeout(
                () => {
                    console.log("Current version", this.version);
                    console.log("Version to compare", version);
                    console.log("asdasdasadasdsad");
                    console.log(fs.readFileSync('data/versions/' + this.version + '.json', 'utf8'));
                    var counting = JSON.parse(fs.readFileSync('data/versions/' + this.version + '.json', 'utf8'));
                    var processItems = function (index, baseVersion, compareVersion) {
                        console.log("procesing", index, baseVersion, compareVersion);
                        if (index >= 0) {
                            const initScreenshot = "screenshots/" + baseVersion + "/" + index + ".png";
                            const finalScreenshot = "screenshots/" + compareVersion + "/" + index + ".png";
                            const changesScreenshot = "screenshots/changes_" + baseVersion + "_" + compareVersion + "_" + index + ".png";
                            resemble(initScreenshot).compareTo(finalScreenshot).onComplete(async function (data) {
                                await fs.writeFile(changesScreenshot, data.getDiffImageAsJPEG());
                                var jsonData = JSON.stringify(data);
                                await fs.writeFile("data/comparisons/" + baseVersion + "_" + compareVersion + "_" + index + ".json", jsonData);
                                processItems(index - 1, baseVersion, compareVersion);

                            });

                        } else {
                            fs.unlinkSync('data/versions/' + baseVersion + '.json');
                            fs.unlinkSync('data/versions/' + compareVersion + '.json');
                            resolve("¡Éxito!");

                        }
                    };

                    processItems(counting.count - 1, this.version, version);
                }, 2000);

        });

    }

    // public async takeInitialScreenshot() {
    //     const initScreenshot = "screenshots/initial_" + idScreenshot + ".png";
    //     await browser.saveScreenshot(initScreenshot);
    // }

    // public async takeFinalScreenshot() {
    //     const finalScreenshot = "screenshots/final_" + idScreenshot + ".png";
    //     await browser.saveScreenshot(finalScreenshot);
    // }

    public async validateScreenshots() {
        // console.log("fin validateScreenshots");
        return new Promise((resolve, reject) => {
            // console.log("validateScreenshots");
            const initScreenshot = "screenshots/initial_" + idScreenshot + ".png";
            const finalScreenshot = "screenshots/final_" + idScreenshot + ".png";
            const changesScreenshot = "screenshots/changes_" + idScreenshot + ".png";
            resemble(initScreenshot).compareTo(finalScreenshot).onComplete(async function (data) {
                await fs.writeFile(changesScreenshot, data.getDiffImageAsJPEG());
                var jsonData = JSON.stringify(data);
                await fs.writeFile("data/" + idScreenshot + ".json", jsonData);
                resolve("¡Éxito!");
            });
        });
    }
}
