var host = "localhost";
var services = ['selenium-standalone'];
let testType = "features";
let app = "pymeadmin";
var headless = false;
var chrome = false;
var firefox = false;
let regressionId = "";

if (process.env.SELENIUM_SERVER) {
    host = process.env.SELENIUM_SERVER;
}
if (process.env.CI_SERVICES) {
    services.push(process.env.CI_SERVICES);
}

process.argv.forEach(function (val, index, array) {
    if (val === "--random") {
        testType = "random";
    }
    if (val === "--features") {
        testType = "features";
    }
    if (val === "--pymeadmin") {
        app = "pymeadmin";
    }
    if (val === "--regression") {
        app = "regression";
    }
    if (val === "--colors") {
        app = "colors";
    }
    if (val === "--adult-services") {
        app = "adult-services";
    }
    if (val === "--los-estudiantes") {
        app = "los-estudiantes";
    }
    console.log("---------------------------------------------");
    console.log(val);
    console.log("---------------------------------------------");
    if (val.includes("--project")) {
        console.log(val);
        let splits = val.split("=");
        let project = splits[1];
        console.log("Project for execution", project);
        app = project;
    }
    if (val.includes("--id")) {
        console.log(val);
        let splits = val.split("=");
        let regression = splits[1];
        console.log("Project for execution", regression);
        regressionId = regression;
    }
    if (val.includes("--headless")) {
        console.log(val);
        let splits = val.split("=");
        headless = splits[1];
        console.log("Is headless", headless);
    }
    if (val.includes("--browsers")) {
        console.log(val);
        let splits = val.split("=");
        if (splits[1] && splits[1].includes("firefox")) {
            firefox = true;
        }
        if (splits[1] && splits[1].includes("chrome")) {
            chrome = true;
        }
        console.log("Browsers", splits);
    }

    // console.log("Key value", val);
});
// console.log("Evaluate app", app);

let baseConfig = {
    host: host,
    port: 4444,
    //
    // ==================
    // Specify Test Files
    // ==================
    // Define which test specs should run. The pattern is relative to the directory
    // from which `wdio` was called. Notice that, if you are calling `wdio` from an
    // NPM script (see https://docs.npmjs.com/cli/run-script) then the current working
    // directory is where your package.json resides, so `wdio` will be called from there.
    //
    // Patterns to exclude.
    exclude: [
        // 'path/to/excluded/files'
    ],
    //
    // ============
    // Capabilities
    // ============
    // Define your capabilities here. WebdriverIO can run multiple capabilities at the same
    // time. Depending on the number of capabilities, WebdriverIO launches several test
    // sessions. Within your capabilities you can overwrite the spec and exclude options in
    // order to group specific specs to a specific capability.
    //
    // First, you can define how many instances should be started at the same time. Let's
    // say you have 3 different capabilities (Chrome, Firefox, and Safari) and you have
    // set maxInstances to 1; wdio will spawn 3 processes. Therefore, if you have 10 spec
    // files and you set maxInstances to 10, all spec files will get tested at the same time
    // and 30 processes will get spawned. The property handles how many capabilities
    // from the same test should run tests.
    //
    maxInstances: 2,
    //
    // If you have trouble getting all important capabilities together, check out the
    // Sauce Labs platform configurator - a great tool to configure your capabilities:
    // https://docs.saucelabs.com/reference/platforms-configurator
    //
    capabilities: [],
    //
    // ===================
    // Test Configurations
    // ===================
    // Define all options that are relevant for the WebdriverIO instance here
    //
    // By default WebdriverIO commands are executed in a synchronous way using
    // the wdio-sync package. If you still want to run your tests in an async way
    // e.g. using promises you can set the sync option to false.
    sync: true,
    //
    // Level of logging verbosity: silent | verbose | command | data | result | error
    logLevel: 'silent',
    //
    // Enables colors for log output.
    coloredLogs: true,
    //
    // Warns when a deprecated command is used
    deprecationWarnings: true,
    //
    // If you only want to run your tests until a specific amount of tests have failed use
    // bail (default is 0 - don't bail, run all tests).
    bail: 0,
    //
    // Saves a screenshot to a given path if a command fails.
    screenshotPath: './errorShots/',
    //
    // Set a base URL in order to shorten url command calls. If your `url` parameter starts
    // with `/`, the base url gets prepended, not including the path portion of your baseUrl.
    // If your `url` parameter starts without a scheme or `/` (like `some/path`), the base url
    // gets prepended directly.
    baseUrl: 'http://localhost',
    //
    // Default timeout for all waitFor* commands.
    waitforTimeout: 60000,
    //
    // Default timeout in milliseconds for request
    // if Selenium Grid doesn't send response
    connectionRetryTimeout: 90000,
    //
    // Default request retries count
    connectionRetryCount: 3,
    //
    // Initialize the browser instance with a WebdriverIO plugin. The object should have the
    // plugin name as key and the desired plugin options as properties. Make sure you have
    // the plugin installed before running any tests. The following plugins are currently
    // available:
    // WebdriverCSS: https://github.com/webdriverio/webdrivercss
    // WebdriverRTC: https://github.com/webdriverio/webdriverrtc
    // Browserevent: https://github.com/webdriverio/browserevent
    plugins: {
        'wdio-screenshot': {}
    },
    // plugins: {
    //     webdrivercss: {
    //         screenshotRoot: 'my-shots',
    //         failedComparisonsRoot: 'diffs',
    //         misMatchTolerance: 0.05,
    //         screenWidth: [320,480,640,1024]
    //     },
    //     webdriverrtc: {},
    //     browserevent: {}
    // },
    //
    // Test runner services
    // Services take over a specific job you don't want to take care of. They enhance
    // your test setup with almost no effort. Unlike plugins, they don't add new
    // commands. Instead, they hook themselves up into the test process.
    services: services,
    // Framework you want to run your specs with.
    // The following are supported: Mocha, Jasmine, and Cucumber
    // see also: http://webdriver.io/guide/testrunner/frameworks.html
    //
    // Make sure you have the wdio adapter package for the specific framework installed
    // before running any tests.
};

if (testType === "random") {
    baseConfig["specs"] = [
        './specs/random.js',
    ];
    baseConfig["framework"] = "jasmine";
    baseConfig["reporters"] = ['spec'];
    baseConfig["jasmineNodeOpts"] = {
        defaultTimeoutInterval: 80000,
        expectationResultHandler: function (passed, assertion) {

        }
    };
} else {
    if (app === "pymeadmin") {
        baseConfig["specs"] = [
            './features/pymeadmin/regresion.feature',
            // './features/pymeadmin/providers.feature',
            // './features/pymeadmin/rest.feature',
        ];
    }
    if (app === "colors") {
        baseConfig["specs"] = [
            './features/colors/screenshot.feature',
        ];
    }
    if (app === "adult-services") {
        baseConfig["specs"] = [
            './features/adult-services/rest.feature',
        ];
    }
    if (app === "los-estudiantes") {
        baseConfig["specs"] = [
            './features/los-estudiantes/e2e/register.feature',
            './features/los-estudiantes/e2e/login.feature',
            './features/los-estudiantes/e2e/qualify.feature',
            './features/los-estudiantes/web-service/rest.feature',
        ];
    }
    if (app === "regression") {
        baseConfig["specs"] = [
            './features/regression/' + regressionId + '/*.feature',
        ];
    }
    if (chrome) {
        let chromeConfig = {
            maxInstances: 1,
            browserName: 'chrome',
            chromeOptions: {
                args: ["--disable-gpu", "--window-size=1400,900"]
            }
        }
        baseConfig.capabilities.push(chromeConfig);
    }
    if (firefox) {
        let firefoxConfig = {
            maxInstances: 1,
            browserName: 'firefox',
            chromeOptions: {
                args: ["--disable-gpu", "--window-size=1400,900"]
            }
        }
        baseConfig.capabilities.push(firefoxConfig);
    }
    console.log(headless === "true");
    console.log(headless);
    if (headless === "true") {
        if (baseConfig.capabilities[0]) {
            baseConfig.capabilities[0].chromeOptions.args.push("--headless");
        }
        if (baseConfig.capabilities[1]) {
            baseConfig.capabilities[1].chromeOptions.args.push("--headless");
        }
    }

    baseConfig["framework"] = "cucumber";
    // baseConfig["reporters"] = ['dot'];
    baseConfig["reporters"] = ['dot', 'multiple-cucumber-html'];
    baseConfig["reporterOptions"] = {
        htmlReporter: {
            jsonFolder: './reports',
            reportFolder: `./reports/report`,
        }
    };
    baseConfig["cucumberOpts"] = {
        require: ['./steps/steps.js', './steps/rest-steps.js'],        // <string[]> (file/dir) require files before executing features
        backtrace: false,   // <boolean> show full backtrace for errors
        compiler: [],       // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
        dryRun: false,      // <boolean> invoke formatters without executing steps
        failFast: false,    // <boolean> abort the run on first failure
        format: ['pretty'], // <string[]> (type[:path]) specify the output format, optionally supply PATH to redirect formatter output (repeatable)
        colors: true,       // <boolean> disable colors in formatter output
        snippets: true,     // <boolean> hide step definition snippets for pending steps
        source: true,       // <boolean> hide source uris
        profile: [],        // <string[]> (name) specify the profile to use
        strict: false,      // <boolean> fail if there are any undefined or pending steps
        tags: [],           // <string[]> (expression) only execute the features or scenarios with tags matching the expression
        timeout: 60000,     // <number> timeout for step definitions
        ignoreUndefinedDefinitions: false, // <boolean> Enable this config to treat undefined definitions as warnings.
    };
}


exports.config = baseConfig;
