//usar los nuevos tipos de variables y características de EMC6
'use strict'

//Dependencies
var https = require('https');
var options = {
    host: 'my.api.mockaroo.com',
    port: 443,
    method: 'GET'
}

function getJsonFile(options, cb){
    var body = '';
    https.request(options, response =>{
        response.on('data', chunk =>{
            body +=chunk;
        });
        response.on('end', ()=>{
            var result = body;
            console.log(`result -->  ${JSON.stringify(result)}`);
            cb(null, result);
        })
        response.on('error', cb);
    }).on('error', cb)
    .end();
}

/*************************************** Get Categories ******************************/
//Obtener Categories
function GetCategories(objrequest, objresponse){
    let categories = '/categories.json?key=05dacb30';
    options.path = categories;
    getJsonFile(options, (err, result)=>{
        if(err){
            console.log(`Error GetCategories ->  ${err}`);
            objresponse.status(404).send({mensaje: `Error consultando mockaroo: ${err}`})
        }
        objresponse.status(200).send(JSON.parse(result));
    })
}

/*************************************** Get Clients ******************************/
//Obtener Clients
function GetClients(objrequest, objresponse){
    let clients = '/clients.json?key=05dacb30';
    options.path = clients;
    getJsonFile(options, (err, result)=>{
        if(err){
            console.log(`Error GetCategories ->  ${err}`);
            objresponse.status(404).send({mensaje: `Error consultando mockaroo: ${err}`})
        }
        objresponse.status(200).send(JSON.parse(result));
    })
}

//Se exporta el controlador
module.exports ={
    GetCategories,
    GetClients
}