'use strict'
/*jshint esversion: 6 */
/* jshint node: true */
var express = require('express'), bodyParser = require('body-parser');
var app = express();
var cors = require('cors');
var fs = require('fs');
//Se instancia el objeto Controller
const mockarooclient = require('./mockaroo')

var multer = require('multer');

var Promise = require('bluebird')
var adb = require('adbkit')
var client = adb.createClient()
var apk = 'mutation-testing/mutants/org.totschnig.myexpenses.debug-mutant1/myExpenses-acra-conscript-debug.apk'
//var apk = 'mutation-testing/myExpenses-acra-conscript-debug.apk'

var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'resources/apks')
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + ".apk")
    }
});
var storageFeatures = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'features/regression')
    },
    filename: (req, file, cb) => {
        console.log("Dest by execution ", file);
        let executionLocation = file.originalname.replace(".feature", "");
        let folder = "features/regression/" + executionLocation;
        if (!fs.existsSync(folder)) {
            fs.mkdirSync(folder);
        }
        cb(null, executionLocation + "/" + Date.now() + ".feature")
    }
});
var upload = multer({ storage: storage });

var uploadFeatures = multer({ storage: storageFeatures });

var corsOptions = {
    "origin": "*",
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
    "preflightContinue": false,
    "optionsSuccessStatus": 204
};
const port = process.env.PORT || 3000;
app.use(cors(corsOptions));
app.use(bodyParser.json());
var exec = require('child_process').exec;


app.get('/', function (req, res) {
    res.send("ok api ready");
});

app.get('/regression', function (req, res) {
    const date = new Date();
    let id = date.getTime();
    dir = exec("id=" + id + " npm run test-regression", function (err, stdout, stderr) {
        if (err) {
            // should have err.code here?
        }
        console.log("stdout ---->", err, stdout, stderr);
        res.send({ id: id, date: date });
    });

    dir.on('exit', function (code) {
        // exit code is code
        console.log("code ---->", code);
    });

});

app.get('/regressions/states/:version/:index', function (req, res) {
    const version = req.params.version;
    const index = req.params.index;
    console.log("Regresions state version", version, index);
    res.sendFile(__dirname + "/screenshots/" + version + "/" + index + ".png");
});

app.get('/regressions/comparison/:baseId/:compareId/:index', function (req, res) {
    const baseId = req.params.baseId;
    const compareId = req.params.compareId;
    const index = req.params.index;
    console.log("Regresions image comparisons", baseId, compareId, index);
    res.sendFile(__dirname + "/screenshots/changes_" + baseId + "_" + compareId + "_" + index + ".png");
});

app.get('/regressions/:baseId/:compareId/:index', function (req, res) {
    const baseId = req.params.baseId;
    const compareId = req.params.compareId;
    const index = req.params.index;
    console.log("Regresions data comparisons", baseId, compareId, index);
    var data = JSON.parse(fs.readFileSync(__dirname + "/data/comparisons/" + baseId + "_" + compareId + "_" + index + ".json", 'utf8'));
    res.send(data);
});

app.get('/reports/:id/monkey', function (req, res) {
    console.log(req.params.id);

    if (fs.existsSync(__dirname + "/seeds/" + req.params.id + ".html")) {
        res.sendFile(__dirname + "/seeds/" + req.params.id + ".html");
    }
    else {
        res.send({ error: "1001" });
    }
});

app.post('/end-to-ends', function (req, res) {
    const endToEnd = req.body;
    console.log(endToEnd);
    console.log("--------------------------------");
    const date = new Date();
    let id = date.getTime();
    const comand = "project=" + endToEnd.project +
        " headless=" + endToEnd.headless +
        " browsers=" + endToEnd.browsers +
        " npm run test";
    console.log("Comand", comand);
    let dir = exec(
        comand,
        function (err, stdout, stderr) {
            if (err) {
                // should have err.code here?
            }
            console.log("stdout ---->", err, stdout, stderr);
            res.send({ id: id, date: date });
        });
    dir.on('exit', function (code) {
        // exit code is code
        console.log("code ---->", code);
    });
});

app.post('/regression/one', uploadFeatures.single('file'), (req, res, next) => {
    console.log('Creating execution for snapshot 1 id ' + req.body.executionId);
    console.log('Saving file name ' + req.file.filename);
    let date = Date.now();
    let id = req.body.executionId;
    let dir = exec("id=" + id + " npm run test-regression", function (err, stdout, stderr) {
        console.log("stdout ---->", err, stdout, stderr);
        rmdir("features/regression/" + id);
        res.send({ id: id, date: date });
    });
});
app.post('/regression/two', uploadFeatures.single('file'), (req, res, next) => {
    console.log('Creating execution for snapshot 2 id ' + req.body.executionId);
    console.log('Saving file name ' + req.file.filename);
    let date = Date.now();
    let id = req.body.executionId;
    let dir = exec("id=" + id + " npm run test-regression", function (err, stdout, stderr) {
        console.log("stdout ---->", err, stdout, stderr);
        rmdir("features/regression/" + id);
        res.send({ id: id, date: date });
    });
});

function rmdir(d) {
    if (fs.existsSync(d)) {
        fs.readdirSync(d).forEach(function (file) {
            var C = d + '/' + file
            fs.unlinkSync(C)
        })
        fs.rmdirSync(d)
    }
}

app.post('/mobile-mutant-testings', upload.single('file'), (req, res, next) => {

    console.log('Saving APK file in: resources/apks/' + req.file.filename);
    console.log('The package is: ' + req.body.package);
    console.log('Number of events for monkey testing is' + req.body.events);
    console.log('Seed for monkey testing' + req.body.seed);

    res.send({ id: req.file.filename });

    console.log('The mutation testing has been started');
    exec("cd mutation-testing && (java -jar MutAPK-0.0.1.jar ./myExpenses-acra-conscript-debug.apk org.totschnig.myexpenses.debug mutants/ MutAPK/extra/ . true)", function (err, stdout, stderr) {
        if (err) {
            // should have err.code here?
        }
        console.log('The mutation testing has been finished');

        exec("calabash-android resign " + apk + "", function (err, stdout, stderr) {

            console.log('The APK has been signed');

            client.listDevices()
                .then(function (devices) {
                    return Promise.map(devices, function (device) {
                        return client.install(device.id, apk)
                    })
                })
                .then(function () {

                    console.log('Installing APK %s', apk)

                    exec("adb shell monkey -p org.totschnig.myexpenses.debug  -s " + req.body.seed + " -v " + req.body.events + " > seeds/" + req.file.filename + ".html", function (err, stdout, stderr) {
                        console.log('The random testing has been finished');
                    });
                })
                .catch(function (err) {
                    console.error('Something went wrong:', err.stack)
                })
        });


    });
});

app.listen(port, function () {
    console.log('Automator tester listening on port !', port);
});

//Mockaroo
/*************************************** CATEGORIES ******************************/

//Obtener usuario por correo para hacer login
app.get('/categories/', mockarooclient.GetCategories)

/*************************************** CLIENTS ******************************/

app.get('/clients/', mockarooclient.GetClients)
