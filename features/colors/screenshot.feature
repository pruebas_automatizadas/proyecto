Feature: Regresion testing for login pyme admin
    I should test pyme admin

    Scenario: Create client
        Given I define a version "v2"
        And I go to the website "https://pymeadmincorp.com/app/#/login"
        And I get element with class "sign-in-button"
        And I take a screenshot
        And I click
        And I get element with name "username"
        And I send text "test"
        And I get element with name "password"
        And I send text "TestTest@1"
        And I get element with class "auth0-lock-submit"
        And I click
        And I get element with id "container_contacts"
        And I take a screenshot
        And I click
        And I get element with id "contacts\/clients"
        And I click
        When I get element with id "create"
        And I click
        And I get element with "class" and value "mat-select-arrow"
        And I take a screenshot
        And I click
        And I get element with tag "span" and content "CC"
        And I click
        And I get element with "formcontrolname" and value "identificationNumber"
        And I send text "2"
        And I get element with "formcontrolname" and value "firstName"
        And I send text "firstName"
        And I get element with "formcontrolname" and value "lastName"
        And I send text "lastName"
        And I take a screenshot