Feature: Testing Users API
    I should test users api

    Scenario: Get all users from api
        Given An api "http://localhost:8080/adult-services/"
        When Make request "GET" to url "users"
        And Add header "Authorization" with value "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UbEJPRGREUVVRMlF6SXlRakV6TVRnek5UbERRemMxTUVSQ09VSkJOVU5FUlRVMlJqZzFSZyJ9.eyJpc3MiOiJodHRwczovL3B5bWVhZG1pbi5hdXRoMC5jb20vIiwic3ViIjoia0paaUFSbE1Mc1JvbFYzUG11b2xvdUszM2tGYjdRc2tAY2xpZW50cyIsImF1ZCI6Imh0dHBzOi8vcHltZWFkbWluLmF1dGgwLmNvbS9hcGkvdjIvIiwiaWF0IjoxNTM2OTYyNzQyLCJleHAiOjE1MzcwNDkxNDIsImF6cCI6ImtKWmlBUmxNTHNSb2xWM1BtdW9sb3VLMzNrRmI3UXNrIiwiZ3R5IjoiY2xpZW50LWNyZWRlbnRpYWxzIn0.aJNTOgn5cu0oZatjpnrEBehZ5AIGLLo516Adgcrv1hXDvVqAh2Vptc87WLE1-pIsYp8CtzLRdHcjUUY6gWfj1BST3Wcz4qy1jZtUJ1fbhoVRoh5yLHNnPUB2dCJH96CblAR_g4p6g6BiAok7ixAkYz8fPcX0X05-w3bYarcXsCOIZ1_zcqBTKJnZNK-PR6iVglPNkm1If_Qz29GFVkZMQSuRvemZ8zlcleULPALe1yDjdYlCOk8XNhCb6k0G6dNuqdnCz9nMBMhnOCsgQ2tMnAOTk0tsWYXRavW15YbjyVvLEVAn5jyWbVtpmsoCqQy_q14kBuRryxT8_9oTd2XKDw"
        And ContentType should be "application/json; charset=utf-8"
        Then Code should be 200
        And Validate request

    Scenario: Create a user on api
        Given An api "http://localhost:8080/adult-services/"
        When Make request "POST" to url "users"
        And Add body '{ "username":"test", "password":"test" }'
        And Add header "Content-Type" with value "application/json"
        Then Code should be 200
        And ContentType should be "application/json; charset=utf-8"
        And Validate request

    Scenario: Update a user on api
        Given An api "http://localhost:8080/adult-services/"
        When Make request "PUT" to url "users/2"
        And Add body '{ "username":"test1", "password":"test1" }'
        And Add header "Content-Type" with value "application/json"
        Then Code should be 200
        And ContentType should be "application/json; charset=utf-8"
        And Validate request