Feature: Register module
    I should test register module

    Scenario: Qualify a teacher
        Given I go to the website "https://losestudiantes.co"
        And I get element with "class" and value "botonCerrar"
        And I click
        And I pause 1000
        And I get element with "class" and value "botonIngresar"
        And I click
        And I get element with "name" and value "nombre" and container with "class" and value "cajaSignUp"
        And I send text "Servio"
        And I get element with "name" and value "apellido" and container with "class" and value "cajaSignUp"
        And I send text "Pantoja"
        And I get element with "name" and value "idUniversidad" and container with "class" and value "cajaSignUp"
        And I click
        And I get element with "value" and value "universidad-de-los-andes" and container with "class" and value "cajaSignUp"
        And I click
        And I get element with "name" and value "idPrograma" and container with "class" and value "cajaSignUp"
        And I click
        And I get element with "value" and value "22" and container with "class" and value "cajaSignUp"
        And I click
        And I get element with "name" and value "correo" and container with "class" and value "cajaSignUp"
        And I send text "shd_cristo@hotmail.com"
        And I get element with "name" and value "password" and container with "class" and value "cajaSignUp"
        And I send text "ServioPantoja@1"
        And I get element with "name" and value "acepta" and container with "class" and value "cajaSignUp"
        And I click
# And I get element with "class" and value "logInButton" and container with "class" and value "cajaSignUp"
# And I click
# Then element with "class" and value "sweet-alert" should exist




