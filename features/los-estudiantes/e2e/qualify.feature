Feature: Qualify module
    I should test qualify module

    Scenario: Qualify a teacher
        Given I go to the website "https://losestudiantes.co"
        And I get element with "class" and value "botonCerrar"
        And I click
        And I pause 1000
        And I get element with "class" and value "botonIngresar"
        And I click
        And I get element with "name" and value "correo" and container with "class" and value "cajaLogIn"
        And I send text "shd_cristo@hotmail.com"
        And I get element with "name" and value "password" and container with "class" and value "cajaLogIn"
        And I send text "ServioPantoja@1"
        And I get element with "class" and value "logInButton" and container with "class" and value "cajaLogIn"
        And I click
        And I pause 1000
        And I get element with "class" and value "Select-control"
        And I click
        And I send text focused "Mario Linares Vasquez"
        And I get element with "class" and value "Select-option"
        And I click
        When I get element with "id" and value "escribir-resena"
        And I click
        And I get element with "name" and value "contenido"
        And I send text focused "Opinion sobre el profesor"
        And I get elements with "class" and value "slider-horizontal"
        And I click
        And I get elements with "name" and value "idMateria"
        And I click
        And I get elements with "value" and value "MISO4208"
        And I click
        #And I get elements with "class" and value "botonPublicar"
        #And I click
        Then element with tag "textarea" text "Opinion sobre el profesor" should exist




