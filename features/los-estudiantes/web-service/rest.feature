Feature: Testing Los estudiantes api API
    I should test providers module

    Scenario: Loggin success
        Given An api "https://api.losestudiantes.co/"
        When Make request "POST" to url "usuarios/signin"
        And Add body '{"correo":"shd_cristo@hotmail.com","password":"ServioPantoja@1"}'
        And Add header "Content-Type" with value "application/json"
        Then Code should be 200
        And ContentType should be "application/json; charset=utf-8"
        And Validate request

    Scenario: Create user that have already exist
        Given An api "https://api.losestudiantes.co/"
        When Make request "POST" to url "usuarios/signup"
        When Add body '{"nombre":"test","apellido":"test","correo":"test@test.com","password":"test@test","idUniversidad":"universidad-de-los-andes","idDepartamento":5,"idPrograma":22,"idPrograma2":null}'
        And Add header "Content-Type" with value "application/json"
        Then Code should be 500
        And ContentType should be "application/json; charset=utf-8"
        And Validate request

    Scenario: Search teacher
        Given An api "https://api.losestudiantes.co/"
        When Make request "GET" to url "universidades/universidad-de-los-andes/administracion/buscar/mario%20linares"
        And Add header "Content-Type" with value "application/json"
        Then Code should be 200
        And ContentType should be "application/json; charset=utf-8"
        And Validate request

    Scenario: Get teacher detail
        Given An api "https://api.losestudiantes.co/"
        When Make request "GET" to url "universidades/universidad-de-los-andes/ingenieria-de-sistemas/profesores/mario-linares-vasquez"
        Then Code should be 200
        And ContentType should be "application/json; charset=utf-8"
        And Validate request
