Feature: Testing for clients module
    I should test clients module

    Scenario: Create client
        Given I go to the website "https://pymeadmincorp.com/app/#/login"
        And I get element with class "sign-in-button"
        And I click
        And I get element with name "username"
        And I send text "test"
        And I get element with name "password"
        And I send text "TestTest@1"
        And I get element with class "auth0-lock-submit"
        And I click
        And I get element with id "container_contacts"
        And I click
        And I get element with id "contacts\/clients"
        And I click
        When I get element with id "create"
        And I click
        And I get element with "class" and value "mat-select-arrow"
        And I click
        And I get element with tag "span" and content "CC"
        And I click
        And I get element with "formcontrolname" and value "identificationNumber"
        And I send text "2"
        And I get element with "formcontrolname" and value "firstName"
        And I send text "firstName"
        And I get element with "formcontrolname" and value "lastName"
        And I send text "lastName"
        And I get element with "id" and value "save"
        And I click
        Then element with tag "div" text "El cliente fue guardado exitosamente" should exist

    Scenario: Delete client
        Given I get element with id "container_contacts"
        And I click
        And I get element with id "contacts\/clients"
        And I click
        When I get element with id "delete_0"
        And I click
        And accept alert
        Then element with tag "div" text "El cliente fue eliminado exitosamente" should exist
