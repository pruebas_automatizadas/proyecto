Feature: Testing Categories API
    I should test providers module

    Scenario: Get all categories from api
        Given An api "https://pymeadmincorp.com/backend/"
        When Make request "GET" to url "categories?pageRecords=1&sizeRecords=10"
        And Add header "Authorization" with value "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UbEJPRGREUVVRMlF6SXlRakV6TVRnek5UbERRemMxTUVSQ09VSkJOVU5FUlRVMlJqZzFSZyJ9.eyJpc3MiOiJodHRwczovL3B5bWVhZG1pbi5hdXRoMC5jb20vIiwic3ViIjoia0paaUFSbE1Mc1JvbFYzUG11b2xvdUszM2tGYjdRc2tAY2xpZW50cyIsImF1ZCI6Imh0dHBzOi8vcHltZWFkbWluLmF1dGgwLmNvbS9hcGkvdjIvIiwiaWF0IjoxNTM3NjUxMzIwLCJleHAiOjE1Mzc3Mzc3MjAsImF6cCI6ImtKWmlBUmxNTHNSb2xWM1BtdW9sb3VLMzNrRmI3UXNrIiwiZ3R5IjoiY2xpZW50LWNyZWRlbnRpYWxzIn0.dbXnI6RiELNoijZWqmIIqXXfq-kjO-Pz1Ida7uRKeY-38eYJyvVYBX0J4fLI8maD6DHM-xHiNphOPmqe-qrrJ-BiBqArttp6BXc25K5bY6JoqEG1SMK_6SJvOqYruw232EhpEQpWAvLEY7EQoBALiXNiW5PqyQDW6gSNvK1SCyihCOf8lMPliPrSx-qXVzZPavanbLpFLWxBgGTOksoJn_LLQICAiXcchzc1bnPNeTyTSPpFYIWjJEC9tA2w8qdS363i9Cb-uZsjNKfhc7rCYkm-Yq1uTRjlksl3OeERzTRWrHTtI1HGCIeXS65Ni4TbFHzEo37qySEDE-9azOeYTA"
        And ContentType should be "application/json;charset=UTF-8"
        Then Code should be 200
        And Validate request

    Scenario: Create a category on api
        Given An api "https://pymeadmincorp.com/backend/"
        When Make request "POST" to url "categories"
        When Add body "{}"
        And Add header "Authorization" with value "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UbEJPRGREUVVRMlF6SXlRakV6TVRnek5UbERRemMxTUVSQ09VSkJOVU5FUlRVMlJqZzFSZyJ9.eyJpc3MiOiJodHRwczovL3B5bWVhZG1pbi5hdXRoMC5jb20vIiwic3ViIjoia0paaUFSbE1Mc1JvbFYzUG11b2xvdUszM2tGYjdRc2tAY2xpZW50cyIsImF1ZCI6Imh0dHBzOi8vcHltZWFkbWluLmF1dGgwLmNvbS9hcGkvdjIvIiwiaWF0IjoxNTM2OTYyNzQyLCJleHAiOjE1MzcwNDkxNDIsImF6cCI6ImtKWmlBUmxNTHNSb2xWM1BtdW9sb3VLMzNrRmI3UXNrIiwiZ3R5IjoiY2xpZW50LWNyZWRlbnRpYWxzIn0.aJNTOgn5cu0oZatjpnrEBehZ5AIGLLo516Adgcrv1hXDvVqAh2Vptc87WLE1-pIsYp8CtzLRdHcjUUY6gWfj1BST3Wcz4qy1jZtUJ1fbhoVRoh5yLHNnPUB2dCJH96CblAR_g4p6g6BiAok7ixAkYz8fPcX0X05-w3bYarcXsCOIZ1_zcqBTKJnZNK-PR6iVglPNkm1If_Qz29GFVkZMQSuRvemZ8zlcleULPALe1yDjdYlCOk8XNhCb6k0G6dNuqdnCz9nMBMhnOCsgQ2tMnAOTk0tsWYXRavW15YbjyVvLEVAn5jyWbVtpmsoCqQy_q14kBuRryxT8_9oTd2XKDw"
        And Add header "Content-Type" with value "application/json"
        Then Code should be 200
        And ContentType should be "application/json;charset=UTF-8"
        And Validate request
