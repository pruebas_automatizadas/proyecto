Feature: Testing for providers module
    I should test providers module

    Scenario: Create provider
        Given I go to the website "https://pymeadmincorp.com/app/#/login"
        And I get element with class "sign-in-button"
        And I click
        And I get element with name "username"
        And I send text "test"
        And I get element with name "password"
        And I send text "TestTest@1"
        When I get element with class "auth0-lock-submit"
        And I click
        And I get element with id "container_contacts"
        And I click
        And I get element with id "contacts\/providers"
        And I click
        And I get element with id "create"
        And I click
        And I get element with "class" and value "mat-select-arrow"
        And I click
        And I get element with tag "span" and content "CC"
        And I click
        And I get element with "formcontrolname" and value "identificationNumber"
        And I send text "001"
        And I get element with "formcontrolname" and value "firstName"
        And I send text "proveedor name"
        And I get element with "formcontrolname" and value "lastName"
        And I send text "proveedor lastName"
        And I get element with "formcontrolname" and value "address"
        And I send text "Carrera 23 44 - 56"
        And I get element with "formcontrolname" and value "phone"
        And I send text "234234234"
        And I get element with "id" and value "save"
        And I click
        Then element with tag "div" text "El proveedor fue guardado exitosamente" should exist

    Scenario: Delete provider
        Given I get element with id "container_contacts"
        And I click
        And I get element with id "contacts\/providers"
        And I click
        When I get element with id "delete_0"
        And I click
        And accept alert
        Then element with tag "div" text "El proveedor fue eliminado exitosamente" should exist
