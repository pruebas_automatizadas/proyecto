import { expect } from "chai";
import { Given, Then } from "cucumber";
const supertest = require('supertest');
const jp = require('jsonpath');
let api;
let request;
let response;
let bucket = [];
// console.log("Adult services environment api ", process.env.URL_ADULT_SERVICES);
Given("An api {string}", async (api) => {
    if (process.env.URL_ADULT_SERVICES) {
        api = process.env.URL_ADULT_SERVICES;
    }
    this.api = supertest(api);
});

Given("Make request {string} to url {string}", async (method: string, url: string) => {
    if (method === "GET") {
        this.request = this.api.get(url);
    }
    if (method === "POST") {
        this.request = this.api.post(url);
    }
    if (method === "PUT") {
        this.request = this.api.put(url);
    }
});

Given("Add header {string} with value {string}", async (header: string, value: string) => {
    this.request.set(header, value);
});

Given("Add body {string}", async (body: string) => {
    this.request.send(body);
});

Given("Save path {string} as {string}", async (path: string, key: string) => {
    bucket[key] = jp.query(this.response, path);
});
Then("Code should be {int}", async (code: number) => {
    return new Promise(async (resolve, reject) => {
        try {
            this.request.expect(code);
            resolve();
        } catch (error) {
            reject(error);
        }
    });
});

Then("ContentType should be {string}", async (contentType: string) => {
    return new Promise(async (resolve, reject) => {
        try {
            this.request.expect('Content-Type', contentType);
            resolve();
        } catch (error) {
            reject(error);
        }
    });
});
Then("Validate request", async () => {
    let self = this;
    return new Promise(async (resolve, reject) => {
        try {
            this.request.end(function (err, res) {
                self.response = res.body;
                resolve();
                if (err) throw err;
            });
        } catch (error) {
            reject(error);
        }
    });
});
