import { expect } from "chai";
import { Given, Then } from "cucumber";
import { Page } from "../page-object/page-object";
let page = new Page();
let element;
let elements;

Given("I go to the website {string}", async (url) => {
    page.goToUrl(url);
});

Given("I get element with class {string}", async (elementClass) => {
    element = await page.findByClass(elementClass);
});

Given("I get element with name {string}", async (elementName) => {
    element = await page.findByName(elementName);
});

Given("I send text {string}", async (text: string) => {
    await element.setValue(text);
});
Given("I send text focused {string}", async (text: string) => {
    await page.sendTextFocused(text);
});

Given("I get element with id {string}", async (elementId) => {
    element = await page.findById(elementId);
});

Given("I click", async () => {
    if (elements) {
        elements.value.forEach(async element => {
            await element.click();
        });
    } else {
        await element.click();
    }
});

Given("accept alert", async () => {
    await page.acceptAlert();
});

Given("I validate screenshots", async () => {
    await page.validateScreenshots();
});

Given("I define a version {string}", async (version: string) => {
    await page.defineVersion(version);
});

Given("Validate with version {string}", async (version: string) => {
    await page.validateVersions(version);
});

Given("I take a screenshot", async () => {
    await page.takeScreenshot();
});

Given("I pause {int}", async (time: number) => {
    await page.pause(time);
});

Given("I get element with {string} and value {string}", async (attribute, value) => {
    element = await page.findByAttribute(attribute, value);
});
Given("I get elements with {string} and value {string}", async (attribute, value) => {
    elements = await page.findAllByAttribute(attribute, value);
    element = undefined;
});

Given("I get element with tag {string} and content {string}", async (tag: string, content: string) => {
    element = await page.findByContent(tag, content);
});

Given("element with tag {string} text {string} should exist", async (tag: string, content: string) => {
    element = await page.findByContent(tag, content);
});

Given("element with {string} and value {string} should exist", async (attribute: string, value: string) => {
    element = await page.findByAttribute(attribute, value);
});

Given("I get element with {string} and value {string} and container with {string} and value {string}",
    async (attribute: string, value: string, containerAttribute: string, containerValue: string) => {
        element = await page.findWithBaseByAttribute(attribute, value, containerAttribute, containerValue);
    });
