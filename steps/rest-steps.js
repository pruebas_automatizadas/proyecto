"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const cucumber_1 = require("cucumber");
const supertest = require('supertest');
const jp = require('jsonpath');
let api;
let request;
let response;
let bucket = [];
// console.log("Adult services environment api ", process.env.URL_ADULT_SERVICES);
cucumber_1.Given("An api {string}", (api) => __awaiter(this, void 0, void 0, function* () {
    if (process.env.URL_ADULT_SERVICES) {
        api = process.env.URL_ADULT_SERVICES;
    }
    this.api = supertest(api);
}));
cucumber_1.Given("Make request {string} to url {string}", (method, url) => __awaiter(this, void 0, void 0, function* () {
    if (method === "GET") {
        this.request = this.api.get(url);
    }
    if (method === "POST") {
        this.request = this.api.post(url);
    }
    if (method === "PUT") {
        this.request = this.api.put(url);
    }
}));
cucumber_1.Given("Add header {string} with value {string}", (header, value) => __awaiter(this, void 0, void 0, function* () {
    this.request.set(header, value);
}));
cucumber_1.Given("Add body {string}", (body) => __awaiter(this, void 0, void 0, function* () {
    this.request.send(body);
}));
cucumber_1.Given("Save path {string} as {string}", (path, key) => __awaiter(this, void 0, void 0, function* () {
    bucket[key] = jp.query(this.response, path);
}));
cucumber_1.Then("Code should be {int}", (code) => __awaiter(this, void 0, void 0, function* () {
    return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
        try {
            this.request.expect(code);
            resolve();
        }
        catch (error) {
            reject(error);
        }
    }));
}));
cucumber_1.Then("ContentType should be {string}", (contentType) => __awaiter(this, void 0, void 0, function* () {
    return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
        try {
            this.request.expect('Content-Type', contentType);
            resolve();
        }
        catch (error) {
            reject(error);
        }
    }));
}));
cucumber_1.Then("Validate request", () => __awaiter(this, void 0, void 0, function* () {
    let self = this;
    return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
        try {
            this.request.end(function (err, res) {
                self.response = res.body;
                resolve();
                if (err)
                    throw err;
            });
        }
        catch (error) {
            reject(error);
        }
    }));
}));
