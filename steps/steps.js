"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const cucumber_1 = require("cucumber");
const page_object_1 = require("../page-object/page-object");
let page = new page_object_1.Page();
let element;
let elements;
cucumber_1.Given("I go to the website {string}", (url) => __awaiter(this, void 0, void 0, function* () {
    page.goToUrl(url);
}));
cucumber_1.Given("I get element with class {string}", (elementClass) => __awaiter(this, void 0, void 0, function* () {
    element = yield page.findByClass(elementClass);
}));
cucumber_1.Given("I get element with name {string}", (elementName) => __awaiter(this, void 0, void 0, function* () {
    element = yield page.findByName(elementName);
}));
cucumber_1.Given("I send text {string}", (text) => __awaiter(this, void 0, void 0, function* () {
    yield element.setValue(text);
}));
cucumber_1.Given("I send text focused {string}", (text) => __awaiter(this, void 0, void 0, function* () {
    yield page.sendTextFocused(text);
}));
cucumber_1.Given("I get element with id {string}", (elementId) => __awaiter(this, void 0, void 0, function* () {
    element = yield page.findById(elementId);
}));
cucumber_1.Given("I click", () => __awaiter(this, void 0, void 0, function* () {
    if (elements) {
        elements.value.forEach((element) => __awaiter(this, void 0, void 0, function* () {
            yield element.click();
        }));
    }
    else {
        yield element.click();
    }
}));
cucumber_1.Given("accept alert", () => __awaiter(this, void 0, void 0, function* () {
    yield page.acceptAlert();
}));
cucumber_1.Given("I validate screenshots", () => __awaiter(this, void 0, void 0, function* () {
    yield page.validateScreenshots();
}));
cucumber_1.Given("I define a version {string}", (version) => __awaiter(this, void 0, void 0, function* () {
    yield page.defineVersion(version);
}));
cucumber_1.Given("Validate with version {string}", (version) => __awaiter(this, void 0, void 0, function* () {
    yield page.validateVersions(version);
}));
cucumber_1.Given("I take a screenshot", () => __awaiter(this, void 0, void 0, function* () {
    yield page.takeScreenshot();
}));
cucumber_1.Given("I pause {int}", (time) => __awaiter(this, void 0, void 0, function* () {
    yield page.pause(time);
}));
cucumber_1.Given("I get element with {string} and value {string}", (attribute, value) => __awaiter(this, void 0, void 0, function* () {
    element = yield page.findByAttribute(attribute, value);
}));
cucumber_1.Given("I get elements with {string} and value {string}", (attribute, value) => __awaiter(this, void 0, void 0, function* () {
    elements = yield page.findAllByAttribute(attribute, value);
    element = undefined;
}));
cucumber_1.Given("I get element with tag {string} and content {string}", (tag, content) => __awaiter(this, void 0, void 0, function* () {
    element = yield page.findByContent(tag, content);
}));
cucumber_1.Given("element with tag {string} text {string} should exist", (tag, content) => __awaiter(this, void 0, void 0, function* () {
    element = yield page.findByContent(tag, content);
}));
cucumber_1.Given("element with {string} and value {string} should exist", (attribute, value) => __awaiter(this, void 0, void 0, function* () {
    element = yield page.findByAttribute(attribute, value);
}));
cucumber_1.Given("I get element with {string} and value {string} and container with {string} and value {string}", (attribute, value, containerAttribute, containerValue) => __awaiter(this, void 0, void 0, function* () {
    element = yield page.findWithBaseByAttribute(attribute, value, containerAttribute, containerValue);
}));
